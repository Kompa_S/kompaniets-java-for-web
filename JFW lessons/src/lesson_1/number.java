package lesson_1;

import java.util.Scanner;

public class number {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Select method and click Enter:");
        System.out.println("1. Switch");
        System.out.println("2. IF");
        System.out.println("3. Array");

        int methodMenuNumber = in.nextInt();

        switch (methodMenuNumber) {
            case 1:
                System.out.println("Input number 0-9 and click Enter");
                int switchInputNumber = in.nextInt();

                switch (switchInputNumber) {
                    case 0:
                        System.out.println("Your number - Zero");
                        break;
                    case 1:
                        System.out.println("Your number - One");
                        break;
                    case 2:
                        System.out.println("Your number - Two");
                        break;
                    case 3:
                        System.out.println("Your number - Three");
                        break;
                    case 4:
                        System.out.println("Your number - Four");
                        break;
                    case 5:
                        System.out.println("Your number - Five");
                        break;
                    case 6:
                        System.out.println("Your number - Six");
                        break;
                    case 7:
                        System.out.println("Your number - Seven");
                        break;
                    case 8:
                        System.out.println("Your number - Eight");
                        break;
                    case 9:
                        System.out.println("Your number - Nine");
                        break;
                    default:
                        System.out.println("bad number");
                }
            case 2:
                System.out.println("Input number 0-9 and click Enter");

                int ifInputNumber = in.nextInt();

                if (ifInputNumber == 0) {
                    System.out.println("Your number - Zero");
                } else if (ifInputNumber == 1) {
                    System.out.println("Your number - One");
                } else if (ifInputNumber == 2) {
                    System.out.println("Your number - two");
                } else if (ifInputNumber == 3) {
                    System.out.println("Your number - three");
                } else if (ifInputNumber == 4) {
                    System.out.println("Your number - four");
                } else if (ifInputNumber == 5) {
                    System.out.println("Your number - five");
                } else if (ifInputNumber == 6) {
                    System.out.println("Your number - six");
                } else if (ifInputNumber == 7) {
                    System.out.println("Your number - seven");
                } else if (ifInputNumber == 8) {
                    System.out.println("Your number - eight");
                } else if (ifInputNumber == 9) {
                    System.out.println("Your number - nine");
                } else System.out.println("bad number");
                break;
            case 3:
                System.out.println("Input number 0-9 and click Enter");

                String num[] = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

                int arrayInputNumber = in.nextInt();

                System.out.println("Your number - " + num[arrayInputNumber]);
                break;
        }
    }
}
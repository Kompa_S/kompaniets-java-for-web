package lesson_1;

import java.util.Scanner;

public class fibon {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("This is program a count fibonacci number, please input you number:");

        int inputNumber = in.nextInt();//считываем целое число a
        int previousNumber = 1;
        int nextNumber = 1;
        int result;

        for (int i = 0; i <= inputNumber; i++) {
            result = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = result;
            System.out.print(result + " ");
        }
        System.out.println();
        System.out.println("Thank you for using the program. Have a nice day! ;)"); //for level up good humor (:
    }
}
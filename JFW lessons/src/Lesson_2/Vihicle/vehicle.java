package Lesson_2.Vihicle;

import Lesson_2.car.gasCar;
import Lesson_2.Boat.powerboat;
import Lesson_2.car.solarPoweredCar;

import java.util.Scanner;

public class vehicle {

    public static void main(String[] args) {
        gasCar gasCar = new gasCar();
        powerboat powerboat = new powerboat();
        solarPoweredCar solarPoweredCar = new solarPoweredCar();

        System.out.println("Select vehicle:");
        System.out.println("1. Car");
        System.out.println("2. Boat");
        System.out.println("3. Solar-powered Car");

        Scanner in = new Scanner(System.in);
        int inputMenuNumber = in.nextInt();

        switch (inputMenuNumber) {
            case 1:
                System.out.println("Select move:");
                System.out.println("1. Accelerate");
                System.out.println("2. Brake");
                System.out.println("3. Turn");

                Scanner acc = new Scanner(System.in);
                int inputMoveCar = acc.nextInt();

                switch (inputMoveCar) {
                    case 1:
                        gasCar.goGasCar();
                        break;
                    case 2:
                        gasCar.brakeGasCar();
                        break;
                    case 3:
                        gasCar.turnGasCar();
                        break;

                }
                break;
            case 2:
                System.out.println("Select move:");
                System.out.println("1. Accelerate");
                System.out.println("2. Brake");
                System.out.println("3. Turn");

                Scanner inBoat = new Scanner(System.in);
                int inputMoveBoat = inBoat.nextInt();

                switch (inputMoveBoat) {
                    case 1:
                        powerboat.goPowerBoat();
                        break;
                    case 2:
                        powerboat.brakePowerBoat();
                        break;
                    case 3:
                        powerboat.turnPowerBoat();
                        break;
                }
                break;
            case 3:
                System.out.println("Select move:");
                System.out.println("1. Accelerate");
                System.out.println("2. Brake");
                System.out.println("3. Turn");

                Scanner inSolarCar = new Scanner(System.in);
                int inputMoveSolarCar = inSolarCar.nextInt();

                switch (inputMoveSolarCar) {
                    case 1:
                        solarPoweredCar.goSolarCar();
                        break;
                    case 2:
                        solarPoweredCar.brakeSolarCar();
                        break;
                    case 3:
                        solarPoweredCar.turnSolarCar();
                        break;

                }
                break;
        }

    }
}
